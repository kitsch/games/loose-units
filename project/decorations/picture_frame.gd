tool
extends Spatial


export(Texture) var texture: Texture setget _set_texture, _get_texture
export(float) var texture_scale: float setget _set_texture_scale, _get_texture_scale

var _ready := false
var _texture: Texture
var _texture_scale: float = 1.0


# Called when the node enters the scene tree for the first time.
func _ready():
	_ready = true
	setup()


func setup():
	if not _texture:
		return

	$Picture.mesh = $Picture.mesh.duplicate(true)
	$Picture.mesh.material = $Picture.mesh.material.duplicate(true)
	$Picture.mesh.material.albedo_texture = _texture
	var size := Vector2(_texture.get_width(), _texture.get_height()) * _texture_scale
	$Picture.scale.x = size.x
	$Picture.scale.z = size.y

	$Corners/TopLeft.translation = Vector3(0, size.y / 200, size.x / 200)
	$Corners/TopRight.translation = Vector3(0, size.y / 200, -size.x / 200)
	$Corners/BottomLeft.translation = Vector3(0, -size.y / 200, size.x / 200)
	$Corners/BottomRight.translation = Vector3(0, -size.y / 200, -size.x / 200)

	$Sides/Left.translation.z = size.x / 200
	$Sides/Left.scale.y = size.y
	$Sides/Right.translation.z = -size.x / 200
	$Sides/Right.scale.y = size.y
	$Sides/Top.translation.y = size.y / 200
	$Sides/Top.scale.y = size.x
	$Sides/Bottom.translation.y = -size.y / 200
	$Sides/Bottom.scale.y = size.x


func _get_texture() -> Texture:
	return _texture


func _set_texture(texture: Texture):
	_texture = texture
	call_deferred("setup")


func _get_texture_scale() -> float:
	return _texture_scale


func _set_texture_scale(texture_scale: float):
	_texture_scale = texture_scale
	call_deferred("setup")
