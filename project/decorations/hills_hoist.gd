extends Useable


const ROTATION_SPEED := 6.66

onready var props = $Props.get_children()
onready var props_left := 0


func _physics_process(delta):
	rotation_degrees.y += delta * ROTATION_SPEED

func _ready():
	for prop in props:
		prop.hide()
		prop.enabled = false
		prop.connect("used", self, "_on_prop_used", [prop])

func show_props():
	print("show")
	for prop in props:
		prop.show()
		prop.enabled = true

	props_left = props.size()

func _on_prop_used(prop: Useable):
	prop.hide()
	prop.enabled = false
	props_left -= 1

	if props_left == 0:
		emit_signal("dialogue_started", passage)
