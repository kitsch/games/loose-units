// Modified by Josef Frank - v1.0.0 for Godot 3.2
// Originally by David Lipps aka DaveTheDev @ EXPWorlds - v2.0.0 for Godot 3.2

shader_type spatial;
render_mode ambient_light_disabled;

const bool use_shade = true;
const bool use_rim = false;
const bool use_light = false;
uniform bool use_shadow = false;

uniform vec4 base_color : hint_color = vec4(1.0);
const vec4 rim_tint = vec4(0.75);

const float shade_threshold = 0.0;
const float shade_softness = 0.002;

const float specular_glossiness = 10.0;
const float specular_threshold = 0.5;
const float specular_softness = 1.0;

const float rim_threshold = 0.25;
const float rim_softness = 0.05;
const float rim_spread = 0.5;

const float shadow_threshold = 0.7;
const float shadow_softness = 0.1;

uniform sampler2D base_texture: hint_albedo;

uniform bool use_specular = false;
uniform vec4 specular_tint : hint_color = vec4(0.75);

void light()
{
	vec4 shade_color = base_color * .666;
	float NdotL = dot(NORMAL, LIGHT);
	float is_lit = step(shade_threshold, NdotL);
	vec3 base = texture(base_texture, UV).rgb * base_color.rgb;
	vec3 shade = texture(base_texture, UV).rgb * shade_color.rgb;
	vec3 diffuse = base;
	
	if (use_shade)
	{
		float shade_value = smoothstep(shade_threshold - shade_softness ,shade_threshold + shade_softness, NdotL);
		diffuse = mix(shade, base, shade_value);
		
		if (use_shadow)
		{
			float shadow_value = smoothstep(shadow_threshold - shadow_softness ,shadow_threshold + shadow_softness, ATTENUATION.r);
			shade_value = min(shade_value, shadow_value);
			diffuse = mix(shade, base, shade_value);
			is_lit = step(shadow_threshold, shade_value);
		}
	}
	
	if (use_specular)
	{
		vec3 half = normalize(VIEW + LIGHT);
		float NdotH = dot(NORMAL, half);
		
		float specular_value = pow(NdotH * is_lit, specular_glossiness * specular_glossiness);
		specular_value = smoothstep(specular_threshold - specular_softness, specular_threshold + specular_softness, specular_value);
		diffuse += specular_tint.rgb * specular_value;
	}
	
	if (use_rim)
	{
		float iVdotN = 1.0 - dot(VIEW, NORMAL);
		float inverted_rim_threshold = 1.0 - rim_threshold;
		float inverted_rim_spread = 1.0 - rim_spread;
		
		float rim_value = iVdotN * pow(NdotL, inverted_rim_spread);
		rim_value = smoothstep(inverted_rim_threshold - rim_softness, inverted_rim_threshold + rim_softness, rim_value);
		diffuse += rim_tint.rgb * rim_value * is_lit;
	}
	
	if (use_light)
	{
		diffuse *= LIGHT_COLOR;
	}
	
	DIFFUSE_LIGHT = diffuse;
}