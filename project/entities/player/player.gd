class_name Player
extends KinematicBody
# The player character.


signal useable_found(useable)

var allow_props := true
var last_useable: Useable
var pitch := 0.0
var view_sensitivity := 0.1

onready var camera: Camera = $Camera
onready var raycast: RayCast = $Camera/RayCast
onready var state_machine: StateMachine = $StateMachine
onready var yaw := rad2deg(rotation.y)


func _ready():
	camera.make_current()
	$Camera/Detector/MetalDetector.hide()

func freeze():
	state_machine.swap("Frozen")

# Rotates the player's camera.
func rotate_camera(intent: Vector2):
	yaw = fmod(yaw - intent.x * view_sensitivity, 360)
	pitch = max(min(pitch - intent.y * view_sensitivity, 85), -85)
	set_rotation(Vector3(0, deg2rad(yaw), 0))
	camera.set_rotation(Vector3(deg2rad(pitch), 0, 0))

func show_metal_detector():
	$Camera/Detector/MetalDetector.show()

func unfreeze():
	state_machine.swap("Moving")

# Checks for Useables in the player's view.
func update_raycast(force_update := false):
	var collider = raycast.get_collider()

	if collider and not (collider is Useable):
		collider = null

	if collider and Input.is_action_just_pressed("click") and allow_props:
		collider.use_left_action()

	if collider == last_useable and not force_update:
		return

	last_useable = collider
	emit_signal("useable_found", collider)
