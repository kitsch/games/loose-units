extends Area


func _physics_process(delta):
	$MetalDetector/Sphere.visible = get_overlapping_areas().size() > 0
