extends "res://classes/state/state.gd"


const ACCELERATION := 15.0
const GRAVITY := 9.8
const JUMP_SPEED := 5.0
const MOVE_SPEED := 2.5
const VIEW_SPEED := 1024.0

var velocity := Vector3.ZERO


func enter():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func exit():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func on_input(event: InputEvent):
	if event is InputEventMouseMotion:
		host.rotate_camera(event.relative)


func on_physics_process(delta: float):
	_process_jumping()
	_process_gravity(delta)
	_process_movement(delta)
	_process_right_stick(delta)
	velocity = host.move_and_slide(velocity, Vector3.UP, true)
	host.update_raycast()


func _process_gravity(delta: float):
	velocity.y -= GRAVITY * delta


func _process_jumping():
	if not host.is_on_floor() or not Input.is_action_just_pressed("jump"):
		return

	velocity.y = JUMP_SPEED


func _process_movement(delta: float):
	var intent = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	).rotated(-host.rotation.y) * MOVE_SPEED
	var vec3 = Vector3(intent.x, velocity.y, intent.y)
	velocity = velocity.linear_interpolate(vec3, ACCELERATION * delta)


func _process_right_stick(delta: float):
	var intent = Vector2(
		Input.get_action_strength("view_right") - Input.get_action_strength("view_left"),
		Input.get_action_strength("view_down") - Input.get_action_strength("view_up")
	) * VIEW_SPEED
	host.rotate_camera(intent * delta)
