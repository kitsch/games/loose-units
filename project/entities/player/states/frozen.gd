extends State


func enter():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func exit():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
