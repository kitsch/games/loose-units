extends PathFollow

const PUSH_SPEED := 20.0

var momentum := 0.0


func _ready():
	unit_offset = 0.0

func _physics_process(delta):
	if unit_offset == 0:
		$StepLadder.enabled = Stats.ute_opened

	offset += momentum * delta
	momentum = lerp(momentum, 0, 12.0 * delta)

	if momentum < 1:
		momentum = 0

	if unit_offset == 1.0:
		$StepLadder.enabled = false
		Stats.ladder_moved = true

func _on_StepLadder_used():
	momentum = PUSH_SPEED

	if unit_offset == 0.0:
		momentum *= 2
