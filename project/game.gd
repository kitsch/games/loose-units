extends Node


onready var cinematic_cameras = $CinematicCameras.get_children()
onready var death_screen = $DeathScreen
onready var dialogue_manager: DialogueManager = $Managers/DialogueManager
onready var huntsman = $Props/Huntsman
onready var player: Player = $Player
onready var props: Node = $Props
onready var task_manager: TaskManager = $Managers/TaskManager
onready var ui: UI = $UI


func _ready():
	$Architecture/Houseplan/Roof.visible = true

	for camera in cinematic_cameras:
		camera.connect("finished", self, "_on_DialogueManager_camera_reset")

	for prop in props.get_children():
		prop.connect("custom_action", self, "_on_Useable_custom_action")
		prop.connect("dialogue_started", dialogue_manager, "play")
		prop.connect("item_given", self, "_on_Useable_item_given")

	Inventory.connect("updated", ui, "set_items")
	ui.set_items(Inventory.list())

func _physics_process(delta):
	$Props/AtticTeleporter.enabled = player.translation.y < 1 and Stats.ladder_moved

func _on_DialogueManager_player_frozen():
	player.freeze()
	ui.set_suspicion_bar_paused(true)

func _on_DialogueManager_player_unfrozen():
	player.unfreeze()
	ui.set_suspicion_bar_paused(false)

func _on_DialogueManager_prop_activated(id: String):
	for prop in props.get_children():
		if prop.name == id:
			prop.enabled = true
			return

func _on_DialogueManager_prop_deactivated(id):
	for prop in props.get_children():
		if prop.name == id:
			prop.enabled = false
			return

func _on_DialogueManager_prop_function_called(id, function):
	props.get_node(id).call(function)

func _on_Player_useable_found(useable: Useable):
	ui.set_prompt(useable.title if useable else "")

func _on_PuzzleManager_puzzle_entered():
	yield(get_tree().create_timer(0.01), "timeout")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_TaskManager_task_finished():
	ui.set_task("")
	ui.stop_suspicion_bar()

func _on_TaskManager_task_requested():
	dialogue_manager.play("DetermineTask")

func _on_TaskManager_task_started(task: Task, is_unique: bool):
	yield(get_tree().create_timer(0.01), "timeout")

	ui.set_task(("Task: %s" % task.title) if task else "")

	if not is_unique:
		ui.start_suspicion_bar()

	if task.passage != "":
		dialogue_manager.play(task.passage)

	if task.name == "RemoveHuntsman":
		huntsman.jump(task.location.position, task.location.rotation)

func _on_UI_time_ran_out():
	_on_DialogueManager_player_frozen()
	death_screen.visible = true
	death_screen.start()
	yield(death_screen, "completed")
	death_screen.hide()
	_on_DialogueManager_player_unfrozen()
	player.translation = Vector3(-15, .23, 3)
	player.pitch = 0
	player.yaw = 0
	player.rotate_camera(Vector2.ZERO)
	ui.suspicion_bar.reset()


func _on_Useable_item_given(item_id: String):
	Inventory.give(item_id)

func _on_Useable_custom_action(action: String):
	match action:
		"do_attic_teleport":
			player.pitch = 0
			player.yaw = -90
			player.rotate_camera(Vector2.ZERO)
			player.translation = Vector3(-4.8, int(player.translation.y < 1) * 3.0, 2.45)

		"do_safe_teleport":
			var diff = ($Props/SafeTeleporter.translation.x - player.translation.x)
			player.translation.x += diff * 1.5


func _on_DialogueManager_props_allowed_set(is_allowed: bool):
	player.allow_props = is_allowed


func _on_DialogueManager_camera_reset():
	ui.crosshair.show()
	player.camera.make_current()
	_on_DialogueManager_player_unfrozen()


func _on_DialogueManager_camera_set(camera_id):
	for camera in cinematic_cameras:
		if camera.name == camera_id:
			ui.crosshair.hide()
			camera.activate(player)
			_on_DialogueManager_player_frozen()


func _on_DialogueManager_metal_detector_shown():
	$Characters/Garbo.translation.x = -24.5
	$Characters/Garbo.rotation_degrees.y = -85
	player.show_metal_detector()


func _on_DialogueManager_prop_destroyed(id):
	for prop in props.get_children():
		if prop.name == id:
			prop.queue_free()
			if ui.prompt.text == prop.title:
				ui.set_prompt("")


func _on_DialogueManager_credits_rolled():
	get_tree().change_scene("res://ui/splashes/credits.tscn")


func _on_DialogueManager_dialogue_began():
	player.allow_props = false


func _on_DialogueManager_dialogue_ended():
	player.allow_props = true
