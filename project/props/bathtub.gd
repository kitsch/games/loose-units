extends Useable


const MAX_LIQUID_SCALE := Vector3(1.0, .95, 1.0)
const MIN_LIQUID_SCALE := Vector3(.835, .002, .93)

var liquid_level := 0.0


func _physics_process(delta):
	$Liquid.scale = MIN_LIQUID_SCALE.linear_interpolate(MAX_LIQUID_SCALE, liquid_level)

func drain():
	liquid_level = 0.0

func fill():
	liquid_level = 1.0
