extends Useable


export(float) var open_angle := 0.0


func open():
	rotation_degrees.y = open_angle
