extends Useable


onready var path: Path = get_node("../../RoombaPath")
onready var path_follow: PathFollow = get_node("../../RoombaPath/PathFollow")
onready var player: Player = get_node("../../Player")
onready var raycast: RayCast = get_node("../../RoombaRayCast")
onready var tween: Tween = $Tween
onready var ui: UI = get_node("../../UI")

var in_scan_mode := false
var stops := [.12, .3, .56, .78, 1.0]
var waiting := false


func enter_scan_mode():
	$AudioStreamPlayer.play()
	in_scan_mode = true


func _physics_process(delta):
	if not waiting:
		return

	raycast.translation = global_transform.origin
	raycast.cast_to = (player.global_transform.origin + Vector3(0, .5, 0)) - raycast.global_transform.origin
	raycast.force_raycast_update()

	var collider = raycast.get_collider()
	if collider and collider is Player:
		waiting = false
		enter_scan_mode()


func move_to_basement_door():
	translation = Vector3(-7, 0, 3.5)
	rotation_degrees.y = 0

func move_to_basement_side():
	translation = Vector3(-9, 0, 2.75)
	rotation_degrees.y = 90

func move_to_backyard_door():
	translation = Vector3(-12, 0, -2.5)
	rotation_degrees.y = -90


func next_stop():
	tween.interpolate_property(path_follow, "unit_offset", null, stops.pop_front(), .25)
	tween.start()
	yield(tween, "tween_completed")

	if not stops.size() == 0:
		waiting = true


func setup_gauntlet():
	get_parent().remove_child(self)
	path_follow.add_child(self)
	self.owner = path_follow
	translation = Vector3.ZERO
	next_stop()


func _on_StepLadder_used():
	if not in_scan_mode:
		return

	$AudioStreamPlayer.stop()
	in_scan_mode = false
	next_stop()
	ui.add_to_suspicion_bar(1.5)


func _on_AudioStreamPlayer_finished():
	print("finished")
	in_scan_mode = false
	next_stop()
