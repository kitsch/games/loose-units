extends Useable

var tune_speed := 4.0
var tuned := true

onready var esb: AudioStreamPlayer3D = $ESBRadio
onready var stat: AudioStreamPlayer3D = $Static


func _physics_process(delta):
	esb.unit_db = linear2db(lerp(db2linear(esb.unit_db), float(tuned), tune_speed * delta))
	stat.unit_db = linear2db(lerp(db2linear(stat.unit_db), float(not tuned), tune_speed * delta))

func detune():
	tuned = false

func tune():
	tuned = true
