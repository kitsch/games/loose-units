extends Node

var killed_huntsman := false
var ladder_moved := false
var tasks_completed := 0
var ute_opened := false


func random(maximum: int) -> int:
	return randi() % maximum + 1
