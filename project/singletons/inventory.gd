extends Node


signal updated(items)

var _items = []


func give(item_id: String):
	_items.append(item_id)
	emit_signal("updated", _items)

func has(item_id: String):
	return _items.has(item_id)

func list() -> Array:
	return _items

func take(item_id: String):
	var i := 0
	for item in _items:
		if item == item_id:
			_items.remove(i)
			emit_signal("updated", _items)
			return true

		i += 1

	return false
