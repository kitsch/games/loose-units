extends Node
class_name Task


export(float) var frequency := 1.0
export(String) var passage := ""
export(String) var title := "<task title>"


# Performed when task is started
func start():
	pass
