extends Node
class_name DialogueManager

signal credits_rolled
signal item_given(id)
signal item_taken(id)
signal metal_detector_shown
signal player_frozen
signal player_killed
signal player_unfrozen
signal prop_activated(id)
signal prop_deactivated(id)
signal prop_destroyed(id)
signal prop_function_called(id, function)
signal props_allowed_set(is_allowed)
signal sound_played(id)
signal suspicion_modified(amount)

signal camera_reset
signal camera_set(camera_id)

signal dialogue_began
signal dialogue_ended
signal dialogue_line_shown(speaker, text)

signal puzzle_disabled
signal puzzle_enabled
signal puzzle_entered(puzzle_id)
signal puzzle_exited

signal common_task_began(task_ids)
signal task_loop_cancelled
signal task_completed
signal task_loop_started
signal unique_task_began(task_id)

const DEFAULT_TEXT_SHOW_TIME := 3.0

onready var timer: Timer = $Timer
onready var voice_line: AudioStreamPlayer = $VoiceLine
onready var yarn_player: YarnPlayer = $YarnPlayer

var items: Array = []


func play(passage_id: String):
	yarn_player.play(passage_id)


func _on_YarnPlayer_command_triggered(command: String, arguments):
	match command:
		# Camera commands
		"reset_camera":
			emit_signal("camera_reset")
		"set_cinematic_camera":
			emit_signal("camera_set", arguments[0])

		# Item commands
		"give_item":
			Inventory.give(arguments[0])
		"take_item":
			Inventory.take(arguments[0])

		# Player commands
		"freeze":
			emit_signal("player_frozen")
		"kill":
			emit_signal("player_killed")
		"unfreeze":
			emit_signal("player_unfrozen")

		# Prop commands
		"activate":
			emit_signal("prop_activated", arguments[0])
		"deactivate":
			emit_signal("prop_deactivated", arguments[0])
		"destroy":
			emit_signal("prop_destroyed", arguments[0])
		"call":
			emit_signal("prop_function_called", arguments[0], arguments[1])
		"disable_using_props":
			emit_signal("props_allowed_set", false)
		"enable_using_props":
			emit_signal("props_allowed_set", true)

		# Puzzle commands
		"disable_puzzle":
			emit_signal("puzzle_disabled")
		"enable_puzzle":
			emit_signal("puzzle_enabled")
		"enter_puzzle":
			emit_signal("puzzle_entered", arguments[0])
		"exit_puzzle":
			emit_signal("puzzle_exited")

		# Task commands
		"cancel_task_loop":
			emit_signal("task_loop_cancelled")
		"complete_task":
			emit_signal("task_completed")
		"start_common_task":
			emit_signal("common_task_began", arguments)
		"start_task_loop":
			emit_signal("task_loop_started")
		"start_unique_task":
			emit_signal("unique_task_began", arguments[0])

		"kill_huntsman":
			Stats.killed_huntsman = true
		"play":
			play_sound(arguments[0])
		"roll_credits":
			get_tree().change_scene("res://ui/splashes/credits.tscn")
		"show_metal_detector":
			emit_signal("metal_detector_shown")
		"suspect":
			emit_signal("suspicion_modified", float(arguments[0]))

	yarn_player.do_next()

func play_sound(id: String):
	var stream: AudioStreamOGGVorbis = load("res://assets/voice_lines/%s.ogg" % id)
	stream.loop = false
	voice_line.stream = stream
	voice_line.play()

func _on_YarnPlayer_dialogue_triggered(speaker, text):
	emit_signal("dialogue_line_shown", speaker, text)

	if not voice_line.playing:
		timer.wait_time = DEFAULT_TEXT_SHOW_TIME
		timer.start()


func _on_YarnPlayer_options_shown(titles):
	pass # Replace with function body.


func _on_YarnPlayer_playback_began():
	emit_signal("dialogue_began")


func _on_YarnPlayer_playback_ended():
	emit_signal("dialogue_ended")
