extends Control

signal passage_requested(id)
signal puzzle_entered
signal puzzle_exited

onready var click_sink: ToolButton = $ClickSink
onready var current_puzzle: Puzzle
onready var puzzles := {}
onready var tween: Tween = $Tween


func _ready():
	modulate.a = 0.0

	for puzzle in $Puzzles.get_children():
		puzzle.hide()

		puzzle.connect("closed", self, "request_passage")
		puzzle.connect("completed", self, "request_passage")
		puzzle.connect("failed", self, "request_passage")

		puzzles[puzzle.name] = puzzle

func disable():
	click_sink.show()

func enable():
	click_sink.hide()

func enter_puzzle(id: String):
	if not puzzles.has(id):
		print("Trying to open non-existent puzzle '%s'." % id)
		return

	current_puzzle = puzzles[id]
	current_puzzle.setup()
	current_puzzle.show()
	modulate.a = 1.0

	emit_signal("puzzle_entered")

func exit_puzzle():
	if not current_puzzle:
		print("Can't close puzzle if none open!")
		return

	current_puzzle.hide()
	current_puzzle = null
	modulate.a = 0.0

	emit_signal("puzzle_exited")

func request_passage(id: String):
	if id == "":
		return

	emit_signal("passage_requested", id)
