extends Task


var location
var locations := [
	{
		"position": Vector3(4.765, 1.8, 4.685),
		"rotation": 90,
		"title": "Main Bedroom",
	},
	{
		"position": Vector3(2.08, 1.4, 3.8),
		"rotation": 0,
		"title": "Side Bedroom",
	},
	{
		"position": Vector3(-2.04, .4, -.37),
		"rotation": -90,
		"title": "Kitchen",
	},
	{
		"position": Vector3(-9.0, 1.2, -3.78),
		"rotation": 180,
		"title": "Bathroom",
	},
]


func start():
	location = locations[randi() % locations.size()]
	title = "Remove the huntsman from the %s." % location.title
