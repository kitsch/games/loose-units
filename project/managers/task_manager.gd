extends Node
class_name TaskManager


signal task_finished
signal task_requested
signal task_started(task, is_unique)

const MIN_TIMER_LENGTH := 5.0
const MAX_TIMER_LENGTH := 15.0

var current_task: Task

onready var common_tasks: Node = $CommonTasks
onready var timer: Timer = $Timer
onready var unique_tasks: Node = $UniqueTasks


func _ready():
	# Set up the random number generator
	randomize()

func build_task_list(task_ids: Array):
	var data := {
		"tasks": [],
		"total_frequency": 0.0,
	}

	for id in task_ids:
		var task = common_tasks.get_node(id)
		data.tasks.append(task)
		data.total_frequency += task.frequency

	return data

func end_task():
	Stats.tasks_completed += 1
	current_task = null
	emit_signal("task_finished")
	start_timer()

# Requests a task.
func request_task():
	emit_signal("task_requested")

# Randomly selects and begins a task.
func start_common_task(task_ids):
	print(task_ids)
	var data = build_task_list(task_ids)
	var value: float = (randf() * data.total_frequency)

	for task in data.tasks:
		if value <= task.frequency:
			current_task = task
			task.start()
			emit_signal("task_started", task, false)
			return

		value -= task.frequency

# Specifically starts a unique task.
func start_unique_task(task_id):
	current_task = unique_tasks.get_node(task_id)
	current_task.start()
	emit_signal("task_started", current_task, true)

# Begins countdown to the start of a task.
func start_timer():
	var difference := MAX_TIMER_LENGTH - MIN_TIMER_LENGTH
	timer.wait_time = MIN_TIMER_LENGTH + difference * randf()
	timer.start()

func stop_timer():
	timer.stop()
