extends Control


var clicked := false
var current := 0


func _ready():
	$Clicker.hide()
	$Title.modulate.a = 0


func _process(delta):
	if $Title.modulate.a == 0 and clicked:
		return

	$Title.modulate.a = clamp($Title.modulate.a + (float(not clicked) * 2 - 1) * delta, 0, 1)
	if $Title.modulate.a == 1:
		$Clicker.show()
	if $Title.modulate.a == 0 and clicked:
		show_slide()


func _on_Clicker_button_down():
	clicked = true
	$Clicker.hide()
	$AudioStreamPlayer.play()


func show_slide():
	for slide in $Screens.get_children():
		slide.hide()

		if slide.get_index() == current:
			slide.show()
			slide.get_node("AudioStreamPlayer").play()
			yield(slide.get_node("AudioStreamPlayer"), "finished")
			yield(get_tree().create_timer(1), "timeout")
			current += 1

			if current < $Screens.get_children().size():
				show_slide()
			else:
				get_tree().change_scene("res://game.tscn")

			return
