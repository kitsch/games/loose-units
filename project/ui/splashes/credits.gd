extends Control


var clicked := false
var current := 0

onready var tween: Tween = $Tween


func _ready():
	$Title.modulate.a = 0.0
	tween.interpolate_property($Title, "modulate:a", 0, 1.0, 2.0)
	tween.start()
	yield(tween, "tween_completed")
	yield(get_tree().create_timer(5.5), "timeout")
	tween.interpolate_property($Title, "modulate:a", 1, 0.0, 2.0)
	tween.start()
	yield(tween, "tween_completed")
	show_slide()


func show_slide():
	var screens = $Screens/CenterContainer.get_children()

	for slide in screens:
		slide.hide()

		if slide.get_index() == current:
			slide.show()
			yield(get_tree().create_timer(4), "timeout")
			current += 1

			if current < screens.size():
				show_slide()
			return
