extends Puzzle


const FREQ := 1101
const FREQ_START := 60
const FREQ_VARIABLE := 2.5
const LEFT := 20
const RIGHT := 1245

var is_sliding := false

onready var bg: ColorRect = $BG
onready var slider: TextureButton = $BG/Slider


func _process(delta):
	if not is_sliding:
		return

	var pos = bg.get_local_mouse_position().x - slider.rect_size.x / 2
	slider.rect_position.x = clamp(pos, LEFT, RIGHT)

	if abs(pos - FREQ) < FREQ_VARIABLE:
		is_sliding = false
		report_success()


func setup():
	slider.rect_position.x = FREQ_START


func _on_Slider_button_down():
	is_sliding = true


func _on_Slider_button_up():
	is_sliding = false
