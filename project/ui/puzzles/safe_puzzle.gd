extends Puzzle


const MAX_LENGTH := 8
const WINNING_COMBO := "80085"

onready var label: Label = $Control/VBoxContainer/ColorRect/Label


func _ready():
	for row in $Control/VBoxContainer.get_children():
		if row is HBoxContainer:
			for child in row.get_children():
				child.connect("pressed", self, "add", [child.text])

func add(value: String):
	match value:
		"CLEAR":
			label.text = ""

		"GO":
			test()

		_:
			if label.text.length() < MAX_LENGTH:
				label.text += value

func setup():
	label.text = ""

func test():
	if label.text != WINNING_COMBO:
		label.text = ""
		report_failure()
		return

	report_success()
