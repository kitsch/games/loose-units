extends Puzzle

onready var bathtub = $Control/ViewportContainer/Viewport/Bathtub

var can_fill := false
var is_filling := false


func setup():
	bathtub.liquid_level = 0.0
	can_fill = true


func _physics_process(delta):
	if not can_fill:
		return

	if not is_filling:
		bathtub.liquid_level = max(0.0, bathtub.liquid_level - delta)
		return

	bathtub.liquid_level = min(1.0, bathtub.liquid_level + delta * .666)

	if bathtub.liquid_level == 1.0:
		can_fill = false
		report_success()

func _on_FillButton_button_down():
	is_filling = true


func _on_FillButton_button_up():
	is_filling = false
