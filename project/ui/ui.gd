extends Control
class_name UI


signal time_ran_out

onready var crosshair: TextureRect = $Crosshair
onready var dialogue_text: FadingText = $DialogueText
onready var inventory: FadingText = $Inventory
onready var prompt: FadingText = $Prompt
onready var suspicion_bar: SuspicionBar = $SuspicionBar
onready var task: FadingText = $Task


func add_to_suspicion_bar(value: float):
	suspicion_bar.add(value)

func reset_suspicion_bar():
	suspicion_bar.reset()

func set_items(items: Array):
	inventory.real_text = PoolStringArray(items).join("\n")

func set_prompt(text: String):
	prompt.real_text = text

func set_suspicion_bar_paused(is_paused: bool):
	suspicion_bar.is_paused = is_paused

func set_suspicion_bar_puzzle_block(is_blocked: bool):
	suspicion_bar.is_puzzle_blocked = is_blocked

func set_task(text: String):
	task.real_text = text

func start_suspicion_bar():
	suspicion_bar.start()

func stop_suspicion_bar():
	suspicion_bar.stop()

# Dialogue functions

func hide_dialogue():
	dialogue_text.real_text = ""

func set_dialogue_text(speaker: String, text: String):
	if speaker:
		var colour: String

		match speaker:
			"Alf_Stewart":
				colour = "#ca6060"
			"Brickie":
				colour = "#c02424"
			"Garbo":
				colour = "#24c024"
			"Huntsman":
				colour = "#24c0c0"
			"Ned_Kelly":
				colour = "#c0c0c0"
			"Roomba":
				colour = "#c024c0"
			"Sparky":
				colour = "#2424c0"
			"_":
				colour = "#c0c0c0"

		dialogue_text.real_text = "[b][color=%s]%s[/color][/b]\n%s" % [colour, speaker.replace("_", " "), text]
		return

	dialogue_text.real_text = text

func show_dialogue():
	dialogue_text.visible = true

func _on_SuspicionBar_time_ran_out():
	emit_signal("time_ran_out")


# Puzzle functions

func enter_puzzle():
	crosshair.hide()
	inventory.hide()
	prompt.hide()
	suspicion_bar.hide()
	task.hide()

func exit_puzzle():
	crosshair.show()
	inventory.show()
	prompt.show()
	suspicion_bar.show()
	task.show()
