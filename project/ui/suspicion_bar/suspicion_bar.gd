extends Control
class_name SuspicionBar


signal time_ran_out

const SPEED := .1
const WAIT_TIME := 6.66

var is_paused := false
var is_puzzle_blocked := false
var phase := 0.0
var wait: float = INF
var was_rising := false

onready var segments = $Segments.get_children()


func _physics_process(delta: float):
	if wait == INF:
		return

	if wait > 0.0:
		wait = max(0.0, wait - delta)
	elif not is_paused and not is_puzzle_blocked:
		phase += delta * SPEED

	for segment in segments:
		segment.phase = clamp(phase - segment.get_index(), 0.0, 1.0)

	if phase >= 3.0:
		stop()
		emit_signal("time_ran_out")

func add(value: float):
	phase = max(floor(phase), phase + value)

func reset():
	phase = 0
	if was_rising or (wait != INF):
		wait = WAIT_TIME

func start():
	wait = WAIT_TIME

func stop():
	was_rising = wait != INF
	wait = INF
	phase = max(0.0, ceil(phase) - 1.0)
