extends ColorRect
class_name SuspicionBarSegment


onready var cover: ColorRect = $Cover

var phase := 0.0


func _process(_delta):
	cover.anchor_left = sin(sin(phase * PI / 2) * PI / 2)
