extends Control


signal completed

var playing := false
var value := 0.0


func _physics_process(delta):
	if not playing:
		return

	value += delta
	update()


func start():
	playing = true
	value = 0
	update()


func update():
	var alpha := min(1.0, value * .4)
	$ColorRect.color.a = alpha
	$ViewportContainer/Viewport/Camera.rotation_degrees.y = -45 + 135 * alpha

	if alpha > .5 and alpha < .9:
		alpha = (alpha - .5) / .4
		$ViewportContainer/Viewport/Brickie.rotation_degrees.y = -45 * alpha
		$ViewportContainer/Viewport/Brickie/Head.rotation_degrees.y = 45 * alpha
		$ViewportContainer/Viewport/Brickie/RightArm.rotation_degrees.z = -135 * alpha
	elif alpha >= .9 and alpha < 1:
		alpha = (alpha - .9) * 10
		$ViewportContainer/Viewport/Brickie.rotation_degrees.y = -60 * (1 - alpha) +15
		$ViewportContainer/Viewport/Brickie/Head.rotation_degrees.y = 60 * (1 - alpha)-15
	elif alpha == 1.0:
		playing = false
		emit_signal("completed")
