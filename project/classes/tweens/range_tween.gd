class_name RangeTween
extends Tween
# A tween which is restricted to between min and max.


signal completed
signal updated(value)

export(float) var fade_time := .5
export(float) var min_value := 0.0
export(float) var max_value := 1.0
export(float) var value := 0.0


func _ready():
	connect("tween_completed", self, "_on_RangeTween_tween_completed")
	connect("tween_step", self, "_on_RangeTween_tween_step")


func fade_to(goal: float):
	if goal == value:
		return

	stop_all()

	goal = clamp(goal, min_value, max_value)
	var time = abs(goal - value) / (max_value - min_value) * fade_time

	interpolate_property(self, "value", value, goal, time)
	start()

	emit_signal("updated", value)


func setup(_time := fade_time, _min := min_value, _max := max_value, _start := value):
	fade_time = _time
	max_value = _max
	min_value = _min
	value = _start


func _on_RangeTween_tween_completed(object, key):
	emit_signal("completed")


func _on_RangeTween_tween_step(object, key, elapsed, _value):
	value = _value
	emit_signal("updated", _value)
