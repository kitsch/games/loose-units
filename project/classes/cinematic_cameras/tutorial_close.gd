extends CinematicCamera


func activate(player):
	.activate(player)
	tween.interpolate_property(self, "fov", null, 3, 32.0)
	tween.start()
