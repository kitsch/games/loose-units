extends CinematicCamera


func activate(player: Player):

	var camera: Camera = player.camera
	var start_pos := camera.global_transform.origin
	var start_rot := camera.global_transform.basis.get_euler()

	translation = start_pos
	rotation = start_rot
	rotation_degrees += Vector3(720, 360, 0)

	.activate(player)
	player.pitch = 0
	player.yaw = 180
	player.rotate_camera(Vector2.ZERO)
	player.translation = Vector3(-32, -.7, -4.4)

	tween.interpolate_property(self, "translation", null, Vector3(-32.5, -.7, -3.9), .75)
	tween.interpolate_property(self, "rotation_degrees", null, Vector3(1, -160, -14), .75)
	tween.start()
	yield(tween, "tween_completed")

	yield(get_tree().create_timer(2), "timeout")

	tween.interpolate_property(self, "translation", null, camera.global_transform.origin, .75)
	tween.interpolate_property(self, "rotation", null, camera.global_transform.basis.get_euler(), .75)
	tween.start()
	yield(tween, "tween_completed")

	emit_signal("finished")
