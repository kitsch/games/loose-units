class_name StateMachine
extends Node
# An implementation of a finite state machine.


export(NodePath) var host := name

var history := []
var state


func _ready():
	state = get_child(0)

	if not state:
		push_error("State machine must have children")
		breakpoint

	_enter_state()


func pop():
	if history.size() == 0:
		push_error("Cannot pop state in state machine '%s'" % name)
		breakpoint

	state.exit()
	state = get_node(history.pop_back())
	_enter_state()


func push(new_state_name: String):
	history.append(state.name)
	state = get_node(new_state_name)
	_enter_state()


func swap(new_state_name: String):
	state.exit()
	state = get_node(new_state_name)
	_enter_state()


func _enter_state():
	state.host = get_node(host)
	state.state_machine = self
	state.enter()


func _input(event):
	state.on_input(event)


func _physics_process(delta):
	state.on_physics_process(delta)


func _process(delta):
	state.on_process(delta)


func _unhandled_input(event):
	state.on_unhandled_input(event)


func _unhandled_key_input(event):
	state.on_unhandled_key_input(event)
