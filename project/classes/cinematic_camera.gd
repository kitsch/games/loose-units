extends Camera
class_name CinematicCamera


signal finished

onready var tween: Tween = Tween.new()


func _ready():
	add_child(tween)
	tween.owner = self

func activate(player: Player):
	make_current()
