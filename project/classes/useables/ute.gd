extends Useable


func use_left_action():
	_set_enabled(false)
	$Model/Tray.rotation_degrees.x = -135
	Stats.ute_opened = true
	Inventory.take("sparky")
	$Model/SparkyUte.show()
	.use_left_action()
