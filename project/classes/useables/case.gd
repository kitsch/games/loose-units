class_name Case
extends Useable


export(PoolStringArray) var default_contents: PoolStringArray
export(String) var description_passage: String

var _inventory: Inventory = Inventory.new()


func _ready():
	add_child(_inventory)

	if not default_contents:
		return

	for id in default_contents:
		_inventory.add(id)


func get_left_action():
	return "Search [EMPTY]" if _inventory.size == 0 else "Search"


#func get_right_action():
#	return "Observe" if description_passage else null


func use_left_action():
	emit_signal("inventory_opened", _inventory)


#func use_right_action():
#	emit_signal("dialogue_started", description_passage)
