class_name ItemPickup
extends Useable


export(String) var item_id: String = ""


func get_left_action():
	return "Pick up"


func use_left_action():
	emit_signal("item_given")
