class_name Useable
extends Area
# An entity which can be used by the player.


signal custom_action(data)
signal destroyed
signal dialogue_started(id)
signal inventory_opened(inventory)
signal item_given(id)
signal item_requested(passage)
signal used

export(bool) var enabled: bool = true setget _set_enabled, _get_enabled
export(String) var passage: String = ""
export(String) var title: String = "SET TITLE"

func destroy():
	emit_signal("destroyed")
	queue_free()

func get_left_action():
	return "Use"

func get_right_action():
	return null

func get_title():
	return title

func start_dialogue(id: String):
	emit_signal("dialogue_started", id)

func use_left_action():
	emit_signal("used")

	if passage != "":
		emit_signal("dialogue_started", passage)

func use_right_action():
	pass

func _get_bitmask():
	return {
		"DISABLED": 256,
		"ENABLED": 512,
	}

func _get_enabled() -> bool:
	return collision_layer != _get_bitmask().DISABLED

func _set_enabled(enabled: bool):
	var BITMASK = _get_bitmask()
	var bitmask = BITMASK.ENABLED if enabled else BITMASK.DISABLED
	collision_layer = bitmask
	collision_mask = bitmask
