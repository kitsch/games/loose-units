class_name Speaker
extends Useable

export(String) var left_action_text: String = "Talk"
export(String) var right_action_text: String = "Give item"
export(String) var give_passage: String = ""
export(String) var talk_passage: String = ""

var inventory: Inventory = Inventory.new()


func get_left_action():
	return left_action_text


func get_right_action():
	return right_action_text


func use_left_action():
	emit_signal("dialogue_started", talk_passage)


func use_right_action():
	emit_signal("inventory_opened", give_passage)
