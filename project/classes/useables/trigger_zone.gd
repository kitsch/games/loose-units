extends Useable


export(bool) var one_shot := false


func use_left_action():
	.use_left_action()

	if one_shot:
		_set_enabled(false)


func _get_bitmask():
	return {
		"DISABLED": 256,
		"ENABLED": 1024,
	}

func _on_TriggerZone_body_entered(body):
	use_left_action()
