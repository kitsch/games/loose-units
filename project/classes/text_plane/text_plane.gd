class_name TextPlane
extends MeshInstance


export(float) var percent_visible: float = 1.0
export(String) var text: String = ""

onready var label: Label = $Viewport/Label


func _ready():
	var viewport_texture: ViewportTexture = material_override.albedo_texture
	viewport_texture.viewport_path = $Viewport.get_path()
	_update()


func _process(delta):
	_update()


func _update():
	label.percent_visible = percent_visible
	label.text = text
