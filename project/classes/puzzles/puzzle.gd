extends Control
class_name Puzzle

signal closed(passage)
signal completed(passage)
signal failed(passage)

export(String) var passage_on_exit := "DefaultPuzzleExit"
export(String) var passage_on_failure := "DefaultPuzzleFailure"
export(String) var passage_on_success := "DefaultPuzzleSuccess"


func setup():
	pass

func close():
	emit_signal("closed", passage_on_exit)

func report_failure():
	emit_signal("failed", passage_on_failure)

func report_success():
	emit_signal("completed", passage_on_success)
