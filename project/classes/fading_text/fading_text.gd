extends RichTextLabel
class_name FadingText


enum ALIGN_TYPE {
	LEFT,
	CENTRE,
	RIGHT,
}

export(ALIGN_TYPE) var align_type = ALIGN_TYPE.LEFT

const FADE_TIME := .25

onready var real_text := text


func _ready():
	modulate.a = float(real_text != "")


func _physics_process(delta: float):
	var align = ["left", "center", "right"][align_type]

	if real_text != "":

		if align == "left":
			bbcode_text = real_text
		else:
			bbcode_text = "[%s]%s[/%s]" % [align, real_text, align]

	modulate.a = lerp(modulate.a, float(real_text != ""), FADE_TIME)
