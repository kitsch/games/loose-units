# Loose Units
Kitsch Digital's SCREAM JAM 2020 Submission

## Yarn Guide

### Checks

*Use these in `<<if>>` statements to determine what to do.*

`Inventory.has("item_id")`  
Determines whether the player has the given item in their inventory.

`Stats.killed_huntsman`  
Determines whether or not the player has killed the huntsman.

`Stats.random(maximum_value)`  
Generates a random number between 1 and the maximum value (inclusive). An example use:
```html
<<set $num to Stats.random(4)>>
<<if $num is 1>>
    Man, that weather is nice.
<<elseif $num is 2>>
    My ass is burning, who gave me the spiced unleaded?
<<else>> <!--This is catches both 3 and 4, so happens half the time-->
    Nice cock, homie.
<<endif>>
```

`Stats.tasks_completed`  
The number of tasks which have been completed so far.

`visited("passage_id")`  
Determines whether a player has accessed the given passage before.

### Commands

*General Commands*

`play <sound_id>`  
Plays a sound file whose length will determine how long the next piece of dialogue will stay on screen.

`suspect <value>`  
Adds or decreases suspicion on the player.

*Camera Commands*

`reset_camera`  
Gives control of the camera back to the player.

`set_cinematic_camera <cinematic_camera_id>`  
Warps to a cinematic camera.

*Inventory Commands*

`give_item <item_id>`  
Puts the item with the given id in the player's inventory.

`take_item <item_id>`  
Removes the item with the given id from the player's inventory.

*Player Commands*

`freeze`   
Prevents the player from moving, rotating the camera, and interacting with objects. Also freezes suspicion bar.

`unfreeze`  
Reverses the effects of `freeze`.

`kill`  
Immediately kills the player.

*Prop Commands*

`activate <prop_name>`  
Allows a prop to be used by the player.

`deactivate <prop_name>`  
Prevents a prop from being used by the player.

`destroy <prop_name>`  
Removes a prop from the world.

`call <prop_name> <function_name>`  
Calls a function on the prop.

`disable_using_props`  
Prevent props from being used at all.

`enable_using_props`  
Allow props to be used.

*Puzzle Commands*

`disable_puzzle`  
Disables editing the puzzle.

`enable_puzzle`  
Enables editing the puzzle.

`enter_puzzle <puzzle_id>`  
Enters a puzzle (pauses suspicion bar).

`exit_puzzle`  
Closes an open puzzle (not a fail state, simply closes it).

*Task Commands*

`cancel_task_loop`  
Cancels tasks from popping up. Assumption is that we will manually start a task (when that task ends, task loop automatically continues).

`complete_task`  
Indicates that a player has successfuly reached the end of a task.

`start_common_task <task_id_1> <task_id_2> ...`  
Starts a random common task from the list given. *Used almost exclusively within the "DetermineTask" passage.*

`start_task_loop`  
Begins the task loop at the start of the game. **ONLY CALL THIS ONCE.**

`start_unique_task <task_id>`  
Starts the given unique task. *Used almost exclusively within the "DetermineTask" passage.*